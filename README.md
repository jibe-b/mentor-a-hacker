# Mentor a hacker

## Description (français)

L'école, l'université, les écoles basées sur la présence à temps plein dans un lieu pour une activité sont adaptées aux personnes ayant un profil proche de l'adolescent·e sans personne à charge et sans plan d'activité précis. 

Pour ceux qui voudraient être formés au plus près du marché, il y a l'apprentissage, et ses atouts en termes d'adaptation des compétences aux attentes effectives des futurs collaborateurs, et surtout employeurs.

Le compagnonage est lui une option pour qui voudrait toucher à de nombreuses techniques, dans des cadres variés.

Mais ce qui nous intéressera dans ce projet, c'est l'apprentissage au fil du temps. Toute situation peut être l'occasion pour une personne, pour peu qu'un petit peu de temps soit disponible et qu'elle ait un peu d'attention à y consacrer, d'apprendre. Et au fil du temps, chacun a l'opportunité d'apprendre sur les domaines les plus variés. 


Ce projet n'est absolument pas révolutionnaire.


Il s'agit d'associer une personne engagée dans un projet à un·e ou des expert·e·s du domaine au cours de l'avancée du projet. L'association proposée est la suivante :

- le développement du projet est mis entre les mains d'une personne et l'intégralité des développements sont versionnés via git
- régulièrement ou ponctuellement pour une demande précise, les expert·e·s réalisent des revues du code et mettent en avant tous les choix d'architecture et de technique pouvant s'avérer problématiques, indiquer les raisons et pointer vers une ou des solutions techniques pouvant permettre d'y remédier

De cette manière, le projet peut démarrer et les premiers prototypes peuvent être créés, tout en ayant le filet de sécurité d'avoir un retour régulier sur les choix ayant été fait tout en ignorant la création d'éventuels problèmes.


Dans cette répartition des rôles, l'entière responsabilité de la feuille de route repose sur le développeur mais la qualité technique du produit est elle mutualisée entre les différents acteurs.


## Position dans le parcours d'un individu

Ce type de tutorat peut être choisi pour un format d'apprentissage (au sens de parcours professionnalisant incluant l'exercice d'une activité professionnelle à temps partiel).

Un tel parcours d'apprentissage peut inclure :
- un partenariat de tutorat tel que décrit ci-dessus, orienté vers l'acquisition de compétences définies par avance
- des partenaires ayant pour intérêt de 

À la fin du parcours d'apprentissage, deux options s'ouvrent :
- l'emploi de l'apprenti·e au sein de l'entreprise de l'un·e des tuteurs
- le lancement de l'activité de l'ancien·ne apprenti·e en indépendant au sein d'une coopérative d'indépendant·e·s



## Contributions

Ce projet est une généralisation a priori du parcours que je vais suivre, à titre individuel.

Je suis à la recherche de personnes souhaitant prendre un rôle soit dans la structuration de ce procédé, soit se placer en tant qu'expert·e pour un·e apprenant.

Contactez-moi sans hésiter [email](mailto:user701@orange.fr)
