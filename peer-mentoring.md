# Peer mentoring rather than reviewing

## Cas particulier

### Presque générique

Pour commencer la réflexion, je vais commencer par mon cas particulier.

Grâce aux contenus disponibles ouvertement sur le web et à un cheminement personnel (autant au niveau de l'épistémo [étude qualitative de certaines pratiques scientifiques] que l'épithymie \[engagement dans le développement d'un cadre favorable à la poursuite d'activités de recherche scientifique [Read more](https://gitlab.com/openscientist/my-own-coop) ]), j'ai pu aboutir à un projet de recherche [[Projet de recherche](https://gitlab.com/jibe-b/doctorat-es-cooperation/issues)].

Ensuite, l'étape suivante est de progresser dans ce projet. Seulement, agir de manière isolée pose des soucis méthodologiques importants auxquels le parti est pris ici de rémédier par la mise en place d'un mentoring suivi.

En effet, premièrement, les résultats et interprétations présentés dans les articles constituant la littérature scientifique sont présentés dans un contexte pour lequel une partie des éléments sont implicites et seront révélés par une discussion avec un·e acteur du domaine.

Deuxièmement, les dynamiques présentes et à venir des groupes de recherche travaillant sur ces questions sont un train en mouvement dont seulement une partie des traces sont déjà disponibles en ligne. Afin d'avoir une activité de recherche à jour sur l'état de l'art et en interaction avec les autres projets de recherche avec lesquels il y a des recoupements, 

Enfin, la rigueur scientifique est un comportement qui est observé et se construit par comparaison, contraste, contradiction et émulation avec d'autres personnes ayant un même objectif de rigueur scientifique.

Pour toutes ces raisons, je fais le choix (et les efforts qui vont avec) de poursuivre le développement de mes projets en open, accessible en ligne en temps presque réel et solliciter des personnes compétentes des domaines desquels participe ce projet de recherche.


### Implémentation

Dans un premier temps, je cherche à me mettre à jour par rapport à l'état de l'art et pour cela, je souhaite combiner la lecture, annotation et rédaction de synthèses avec des échanges directs avec une ou plusieurs personnes des domaines desquels participe ce projet. De cette manière, les savoirs que je vais acquérir le seront en interaction avec l'expérience, les connaissances de personnes aguéries du domaine.

Par ailleurs, l'un des enjeux de la création d'interactions suivies avec des personnes du domaine de mon projet de recherche est de créer du couplage entre les dynamiques de l'une et l'autre des parties, afin de créer le terreau pour des interactions riches, où les parties se connaissent suffisament pour que lorsque l'occasion se présente de réaliser un bout de chemin en commun, cela se fasse en commun et en ayant déjà établi les connexions possibles entre les deux travailleurs.




### Quelles formes de mentorat ?

L'une des formes possibles de mentorat, relativement informelle, est celle des relecteurs intéressés réguliers. Un projet de recherche peut intéresser un groupe de personnes, qui pourront trouver un intérêt à ce que ce projet progresse et atteigne 



